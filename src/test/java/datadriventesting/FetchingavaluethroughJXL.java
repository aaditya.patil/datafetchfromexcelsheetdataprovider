package datadriventesting;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class FetchingavaluethroughJXL {

	public static void main(String[] args) throws BiffException, IOException {

		File f=new File("/home/aaditya.patil/Documents/testdata.xls");
		FileInputStream fis= new FileInputStream(f);

		Workbook book=Workbook.getWorkbook(fis);
		Sheet sh=book.getSheet("Sheet1");
		String celldata=sh.getCell(1, 2).getContents();
		System.out.println("Value Present is:" +celldata);

		int rowCount= sh.getRows();
		int columnsCount=sh.getColumns();

		for(int i=0;i<rowCount;i++)
		{
			for(int j=0;j<columnsCount;j++)
			{
				celldata=sh.getCell(j, i).getContents();
				System.out.println(celldata);
			}

		}}
}
