package datadriventesting;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class AssignmentsDataFetchApachePoi {

	public static void main(String[] args) throws IOException {

		WebDriver driver = new ChromeDriver();

		driver.get("https://demowebshop.tricentis.com/register");

		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));	

		File f= new File("/home/aaditya.patil/Documents/TestData2.xlsx");
		
		FileInputStream fis= new FileInputStream(f);
		
		XSSFWorkbook workbook = new XSSFWorkbook(fis);
		
		XSSFSheet sheet = workbook.getSheetAt(1);
		
		int rows= sheet.getPhysicalNumberOfRows();
		
		for(int i=1; i<rows; i++)
		{
			String firstname= sheet.getRow(i).getCell(0).getStringCellValue();
			
			String lastname= sheet.getRow(i).getCell(1).getStringCellValue();
			
			String email= sheet.getRow(i).getCell(2).getStringCellValue();
			
			String password= sheet.getRow(i).getCell(3).getStringCellValue();
            
			driver.findElement(By.id("FirstName")).sendKeys(firstname);
			
			driver.findElement(By.id("LastName")).sendKeys(lastname);
			
			driver.findElement(By.id("Email")).sendKeys(email);
			
			driver.findElement(By.id("Password")).sendKeys(password);
			
			driver.findElement(By.id("ConfirmPassword")).sendKeys(password);
			
			driver.findElement(By.id("register-button")).click();
		}

	}

}
