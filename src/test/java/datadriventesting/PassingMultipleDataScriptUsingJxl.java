package datadriventesting;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class PassingMultipleDataScriptUsingJxl {

	public static void main(String[] args) throws BiffException, IOException {
		
		WebDriver driver = new ChromeDriver();

		driver.get("https://demowebshop.tricentis.com/login");

		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		
		File f=new File("/home/aaditya.patil/Documents/testdata.xls");
		FileInputStream fis= new FileInputStream(f);

		Workbook book=Workbook.getWorkbook(fis);
		Sheet sh=book.getSheet("Sheet1");
		//String celldata=sh.getCell(1, 2).getContents();
	//	System.out.println("Value Present is:" +celldata);

		int rowCount= sh.getRows();
		int columnsCount=sh.getColumns();
		
		for(int i=1; i<rowCount; i++)
		{
			String username= sh.getCell(0,i).getContents();
			String password= sh.getCell(1,i).getContents();

		driver.findElement(By.id("Email")).sendKeys(username);

		driver.findElement(By.id("Password")).sendKeys(password);

		driver.findElement(By.xpath("//input[@value='Log in']")).click();
		
		driver.findElement(By.linkText("Log out")).click();
		
		
		}
	}

}
