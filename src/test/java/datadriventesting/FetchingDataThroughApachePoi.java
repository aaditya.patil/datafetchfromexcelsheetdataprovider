package datadriventesting;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class FetchingDataThroughApachePoi {

	public static void main(String[] args) throws IOException {
		
		File f= new File("/home/aaditya.patil/Documents/TestData2.xlsx");
		
		FileInputStream fis= new FileInputStream(f);
		
		XSSFWorkbook workbook = new XSSFWorkbook(fis);
		
		XSSFSheet sheet = workbook.getSheetAt(0);
		
		int rows= sheet.getPhysicalNumberOfRows();
		
		for(int i=0; i<rows; i++)
		{
			int column= sheet.getRow(i).getLastCellNum();
			
			for(int j=0; j<column; j++)
			{
				String cellvalue=sheet.getRow(i).getCell(j).getStringCellValue();
				
				System.out.println(cellvalue);
			}
		}
	}

}
